﻿using UnityEngine;
using System.Collections;

public enum GameState {
	TITLE,
	GAME,
	RESULT,
}

public class GameManager : MonoBehaviour {

	public static GameManager Instance {
		get {
			return _instance;
		}
	}
	private static GameManager _instance;

	public GameState CurrentGameState;
	public GameObject TitleUI;
	public GameObject GameUI;
	public GameObject ResultUI;

	void Awake () {
		_instance = this;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		switch(CurrentGameState) {
		case GameState.TITLE:
			TitleInput();
			break;
		case GameState.GAME:
			break;
		case GameState.RESULT:
			break;
		}
	}

	void TitleInput () {
		if(Input.GetKeyDown(KeyCode.Mouse0)) {

		}
	}
}
