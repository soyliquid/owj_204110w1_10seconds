﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

	public Vector3 Speed;

	// Update is called once per frame
	void Update () {
		transform.Rotate(transform.up, Mathf.Sin(Time.time * Speed.y));
		transform.Rotate(transform.right,  Mathf.Sin(Time.time *Speed.x));
        transform.Rotate(transform.forward,  Mathf.Sin(Time.time *Speed.z));
	}
}
